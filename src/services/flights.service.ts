import { Database } from '../databases/database_abstract'
import { DatabaseInstanceStrategy } from '../database'

export class FlightsService {
    private readonly _db: Database

    constructor() {
        this._db = DatabaseInstanceStrategy.getInstance()
    }

    public async getFlights() {
        return this._db.flightRepository.getFlights()
    }

    public async updateFlightStatus(code: string) {
        return this._db.flightRepository.updateFlightStatus(code, 'new')
    }

    public async addFlight(flight: {
        code: string
        origin: string
        destination: string
        status: string
    }) {
        return this._db.flightRepository.addFlight(flight)
    }
}
