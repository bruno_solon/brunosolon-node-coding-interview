import { Database } from '../databases/database_abstract'
import { DatabaseInstanceStrategy } from '../database'
import { LinkPassenger } from '../dto/request/LinkPassenger'

export class PassengersService {
    private readonly _db: Database

    constructor() {
        this._db = DatabaseInstanceStrategy.getInstance()
    }

    public async getPassangers() {}

    public async getPassengerById(id: number) {}

    public async linkPassangerToFlight(request: LinkPassenger) {
        const flightExists = await this._db.flightRepository.getFlightByCode(
            request.flightCode
        )
        if (!flightExists) throw new Error('Flight not found')

        // TODO verify if passenger exists and then link it to the flight
    }
}
