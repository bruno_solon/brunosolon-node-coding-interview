import { Flight } from '../dto/request/Flight'

export interface IFlightRepository {
    addFlight(flight: Flight): Promise<void>
    updateFlightStatus(code: string, status: string): Promise<boolean>
    getFlights(): Promise<Flight[]>
    getFlightByCode(code: string): Promise<Flight>
}
