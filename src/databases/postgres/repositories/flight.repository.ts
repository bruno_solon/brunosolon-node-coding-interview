import { IFlightRepository } from '../../IFlightRepository'
import { Flight } from '../../../dto/request/Flight'
import { IMemoryDb } from 'pg-mem'

export class FlightRepository implements IFlightRepository {
    constructor(private readonly db: IMemoryDb) {}
    public async addFlight(flight: Flight): Promise<void> {
        try {
            await this.db.public.none(
                `INSERT INTO flights (code, origin, destination, status) VALUES ('${flight.code}', '${flight.origin}', '${flight.destination}', '${flight.status}')`
            )
        } catch (err) {
            throw new Error('Error while adding flight')
        }
    }

    public async getFlights(): Promise<Flight[]> {
        return this.db.public.many('SELECT * FROM flights')
    }

    public async updateFlightStatus(
        code: string,
        status: string
    ): Promise<boolean> {
        return Promise.resolve(false)
    }

    public async getFlightByCode(code: string): Promise<Flight> {
        const flight = await this.db.public.one(`
            SELECT * FROM flights WHERE code = '${code}'
        `)

        return {
            code: flight.code,
            origin: flight.origin,
            destination: flight.destination,
            status: flight.status,
        } as Flight
    }
}
