import { IFlightRepository } from './IFlightRepository'
import { IPassengerRepository } from './IPassengerRepository'

export class Database {
    public static _instance: any
    public flightRepository: IFlightRepository
    public passengerRepository: IPassengerRepository

    public static getInstance() {
        // subclass must implement this method
    }
}
