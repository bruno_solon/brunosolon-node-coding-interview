import { JsonController, Get, Post, Body } from 'routing-controllers'
import { PassengersService } from '../services/passengers.service'
import { LinkPassenger } from '../dto/request/LinkPassenger'

@JsonController('/passengers', { transformResponse: false })
export default class PassangersController {
    private _passengerService: PassengersService

    constructor() {
        this._passengerService = new PassengersService()
    }

    @Get('')
    async getAll() {
        try {
            // TODO call service
        } catch (err) {
            return { status: 500, message: '' }
        }
    }

    // add passenger link flight
    // TODO add a body validation
    @Post('')
    async linkPassangerToFlight(
        @Body()
        link: LinkPassenger
    ) {
        try {
            // TODO call service
        } catch (err) {
            return { status: 500, message: '' }
        }
    }
}
