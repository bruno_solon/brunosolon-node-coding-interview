import {
    JsonController,
    Get,
    Param,
    Put,
    Post,
    Body,
} from 'routing-controllers'
import { FlightsService } from '../services/flights.service'
import { Flight } from '../dto/request/Flight'

@JsonController('/flights', { transformResponse: false })
export default class FlightsController {
    private _flightsService: FlightsService

    constructor() {
        this._flightsService = new FlightsService()
    }

    @Get('')
    async getAll() {
        try {
            const response = await this._flightsService.getFlights()

            return {
                status: 200,
                data: response,
            }
        } catch (err) {
            return { status: 500, message: '' }
        }
    }

    @Put('/:code')
    async updateFlightStatus(@Param('code') code: string) {
        try {
            const response = await this._flightsService.updateFlightStatus(code)

            return {
                status: 200,
                data: response,
            }
        } catch (err) {
            return { status: 500, message: '' }
        }
    }

    // add flight
    // TODO add a body validation
    @Post('')
    async addFlight(
        @Body()
        flight: Flight
    ) {
        try {
            const response = await this._flightsService.addFlight(flight)

            return {
                status: 200,
                data: response,
            }
        } catch (err) {
            return { status: 500, message: '' }
        }
    }
}
