export interface Flight {
    code: string
    origin: string
    destination: string
    status: string
}
